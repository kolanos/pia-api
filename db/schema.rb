# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170718021716) do

  create_table "charges", force: :cascade do |t|
    t.integer "subscriber_id"
    t.decimal "amount", precision: 10, scale: 2
    t.integer "transaction_type", default: 0
    t.boolean "processed", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["subscriber_id"], name: "index_charges_on_subscriber_id"
  end

  create_table "subscribers", force: :cascade do |t|
    t.string "email"
    t.integer "payment_processor", default: 0
    t.integer "payment_plan", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "next_bill"
  end

end
