class AddNextBillToSubscribers < ActiveRecord::Migration[5.1]
  def change
    add_column :subscribers, :next_bill, :date, :null => true
  end
end
