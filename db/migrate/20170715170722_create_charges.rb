class CreateCharges < ActiveRecord::Migration[5.1]
  def change
    create_table :charges do |t|
      t.references :subscriber, foreign_key: true
      t.decimal :amount, precision: 10, scale: 2
      t.integer :transaction_type, default: 0
      t.boolean :processed, default: false
      t.timestamps
    end
  end
end
