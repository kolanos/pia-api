class CreateSubscribers < ActiveRecord::Migration[5.1]
  def change
    create_table :subscribers do |t|
      t.string :email
      t.integer :payment_processor, default: 0
      t.integer :payment_plan, default: 0
      t.timestamps
    end
  end
end
