class PaypalController < ApplicationController
  include ExceptionHandler
  include Response

  before_action :set_subscriber

  def create
    @subscriber.charges.create!(charge_params)
    head :no_content
  end

  private

  def charge_params
    params.require(:amount)
    params.require(:transaction_type)
    params.permit(:amount, :transaction_type)
  end

  def set_subscriber
    @subscriber = Subscriber.find_by!(id: params[:subscriber_id], payment_processor: Subscriber.payment_processors[:paypal])
  end
end
