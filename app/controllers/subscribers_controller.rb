class SubscribersController < ApplicationController
  include ExceptionHandler
  include Response

  before_action :set_subscriber, only: [:show, :update, :destroy]

  # GET /subscribers
  def index
    @subscribers = Subscriber.all
    json_response(@subscribers)
  end

  # POST /subscribers
  def create
    @subscriber = Subscriber.create!(subscriber_params)
    json_response(@subscriber, :created)
  end

  # GET /subscribers/:id
  def show
    json_response(@subscriber)
  end

  # PUT /subscribers/:id
  def update
    @subscriber.update(subscriber_params)
    head :no_content
  end

  # DELETE /subscribers/:id
  def destroy
    @subscriber.destroy
    head :no_content
  end

  private

  def subscriber_params
    params.permit(:email)
  end

  def set_subscriber
    @subscriber = Subscriber.find(params[:id])
  end
end
