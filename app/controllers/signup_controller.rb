class SignupController < ApplicationController
  include ExceptionHandler
  include Response

  def create
    @subscriber = Subscriber.find_by!(email: params[:email])
    json_response({ :subscriber_id => @subscriber.id })
  end
end
