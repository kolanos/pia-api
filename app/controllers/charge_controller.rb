class ChargeController < ApplicationController
  include ExceptionHandler
  include Response

  before_action :set_subscriber

  def create
    # This is just a mock
    head :no_content
  end

  private

  def set_subscriber
    @subscriber = Subscriber.find(params[:id])
  end
end
