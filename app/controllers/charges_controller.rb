class ChargesController < ApplicationController
  include ExceptionHandler
  include Response

  before_action :set_subscriber
  before_action :set_charge, only: [:show, :update, :destroy]

  # GET /subscribers/:subscriber_id/charges
  def index
    json_response(@subscriber.charges)
  end

  # GET /subscribers/:subscriber_id/charges/:id
  def show
    json_response(@charge)
  end

  # POST /subscribers/:subscriber_id/charges
  def create
    @subscriber.charges.create!(charge_params)
    json_response(@subscriber, :created)
  end

  # PUT /subscribers/:subscriber_id/charges/:id
  def update
    @charge.update(charge_params)
    head :no_content
  end

  # DELETE /subscribers/:subscriber_id/charges/:id
  def destroy
    @charge.destroy
    head :no_content
  end

  private

  def charge_params
    params.permit(:amount, :transaction_type)
  end

  def set_subscriber
    @subscriber = Subscriber.find(params[:subscriber_id])
  end

  def set_charge
    @charge = @subscriber.charges.find_by!(id: params[:id]) if @subscriber
  end
end
