class Subscriber < ApplicationRecord
  enum payment_processor: { creditcard: 0, paypal: 1 }
  enum payment_plan: { monthly: 0, yearly: 1 }

  has_many :charges, dependent: :destroy

  validates :email, presence: true, uniqueness: true

  def bill
    puts "Billing #{email} with #{payment_plan} plan"

    if self.next_bill.nil?
      self.next_bill = Date.today
      transaction_type = Charge.transaction_types[:signup]
    else
      transaction_type = Charge.transaction_types[:renewal]
    end

    if self.yearly?
      self.next_bill = self.next_bill + 1.year
      amount = 120.00
    else
        self.next_bill = self.next_bill + 1.month
        amount = 10.00
    end

    self.charges.create({ :amount => amount, :transaction_type => transaction_type })
    self.save
  end
end
