require 'credit_card_processor'

class Charge < ApplicationRecord
  belongs_to :subscriber

  enum transaction_type: { signup: 0, renewal: 1, failed_charge: 2 }

  validates :amount, presence: true

  def process
    puts "Processing #{subscriber.email} ##{id} $#{amount}"

    if self.subscriber.creditcard?
      processor = CreditCardProcessor.new
      if self.signup?
        success = processor.signup(self.subscriber.email, self.amount)
        SubscriberMailer.signup_email(self.subscriber).deliver_later
      else
        success = processor.charge(self.subscriber, self.amount)
      end

      if not success
        update({ :transaction_type => Charge.transaction_types[:failed_charge] })
        ChargeMailer.failed_charge_email(self.subscriber).deliver_later
      end
    elsif self.subscriber.paypal?
      if self.failed_charge?
        ChargeMailer.failed_charge_email(self.subscriber).deliver_later
      end
    end

    update({ :processed => true })
  end
end
