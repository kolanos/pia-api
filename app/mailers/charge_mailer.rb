class ChargeMailer < ApplicationMailer
  default from: 'noreply@example.com'

  def failed_charge_email subscriber
    @subscriber = subscriber
    mail(to: @subscriber.email, subject: 'Failed Payment')
  end
end
