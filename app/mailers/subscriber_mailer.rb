class SubscriberMailer < ApplicationMailer
  default from: 'noreply@example.com'

  def signup_email subscriber
    @subscriber = subscriber
    mail(to: @subscriber.email, subject: 'Welcome')
  end
end
