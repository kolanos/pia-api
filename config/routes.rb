Rails.application.routes.draw do
  resources :subscribers do
    resources :charges
  end

  # Webhooks
  post '/signup', to: 'signup#create', as: 'signup'
  post '/charge/:id', to: 'charge#create', as: 'charge'
  post '/paypal', to: 'paypal#create', as: 'paypal'
end
