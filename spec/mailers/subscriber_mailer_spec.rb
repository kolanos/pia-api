require "rails_helper"

RSpec.describe SubscriberMailer, type: :mailer do
  describe 'signup email' do
    let!(:subscriber) { create(:subscriber) }
    let(:mail) { described_class.signup_email(subscriber).deliver_now }

    it 'renders the subject' do
      expect(mail.subject).to eq('Welcome')
    end

    it 'renders the receiver email' do
      expect(mail.to).to eq([subscriber.email])
    end

    it 'renders the sender email' do
      expect(mail.from).to eq(['noreply@example.com'])
    end

    it 'assigns @subscriber' do
      expect(mail.body.encoded).to match(subscriber.email)
    end
  end
end
