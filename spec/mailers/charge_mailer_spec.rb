require "rails_helper"

RSpec.describe ChargeMailer, type: :mailer do
  describe 'failed charge email' do
    let!(:subscriber) { create(:subscriber) }
    let(:mail) { described_class.failed_charge_email(subscriber).deliver_now }

    it 'renders the subject' do
      expect(mail.subject).to eq('Failed Payment')
    end

    it 'renders the receiver email' do
      expect(mail.to).to eq([subscriber.email])
    end

    it 'renders the sender email' do
      expect(mail.from).to eq(['noreply@example.com'])
    end
  end
end
