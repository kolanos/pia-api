require 'rails_helper'

RSpec.describe 'Signup API' do
  let!(:subscriber) { create(:subscriber) }
  let(:subscriber_id) { subscriber.id }
  let(:subscriber_email) { subscriber.email }

  # Test suite for POST /signup
  describe 'POST /signup' do
    let(:valid_attributes) { { email: subscriber_email } }

    before { post "/signup", params: valid_attributes }

    context 'when request is valid' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns the subscriber_id' do
        expect(json).not_to be_empty
        expect(json['subscriber_id']).to eq(subscriber_id)
      end
    end
  end
end
