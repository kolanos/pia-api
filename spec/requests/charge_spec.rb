require 'rails_helper'

RSpec.describe 'Charge API' do
  let!(:subscriber) { create(:subscriber) }
  let(:subscriber_id) { subscriber.id }

  # Test suite for POST /charge/:subscriber_id
  describe 'POST /charge/:subscriber_id' do
    let(:valid_attributes) { { amount: '10.00', transaction_type: Charge.transaction_types[:renewal] } }

    before { post "/charge/#{subscriber_id}", params: valid_attributes }

    context 'when request is valid' do
      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end
end
