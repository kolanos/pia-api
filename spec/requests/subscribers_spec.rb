require 'rails_helper'

RSpec.describe 'Subscribers API', type: :request do
  # initialize test data 
  let!(:subscribers) { create_list(:subscriber, 10) }
  let(:subscriber_id) { subscribers.first.id }

  # Test suite for GET /subscribers
  describe 'GET /subscribers' do
    before { get '/subscribers' }

    it 'returns subscribers' do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  # Test suite for GET /subscribers/:id
  describe 'GET /subscribers/:id' do
    before { get "/subscribers/#{subscriber_id}" }

    context 'when the record exists' do
      it 'returns the subscriber' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(subscriber_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:subscriber_id) { 100 }

      it 'returns status code 403' do
        expect(response).to have_http_status(403)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Subscriber/)
      end
    end
  end

  # Test suite for POST /subscribers
  describe 'POST /subscribers' do
    let(:valid_attributes) { { email: 'foo@bar.com' } }

    context 'when the request is valid' do
      before { post '/subscribers', params: valid_attributes }

      it 'creates a subscriber' do
        expect(json['email']).to eq('foo@bar.com')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/subscribers', params: { email: '' } }

      it 'returns status code 403' do
        expect(response).to have_http_status(403)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Validation failed: Email can't be blank/)
      end
    end
  end

  # Test suite for PUT /subscribers/:id
  describe 'PUT /subscribers/:id' do
    let(:valid_attributes) { { email: 'bar@baz.com' } }

    context 'when the record exists' do
      before { put "/subscribers/#{subscriber_id}", params: valid_attributes }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  # Test suite for DELETE /subscribers/:id
  describe 'DELETE /subscribers/:id' do
    before { delete "/subscribers/#{subscriber_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end
