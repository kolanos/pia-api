require 'rails_helper'

RSpec.describe 'Paypal API' do
  let!(:subscriber) { create(:subscriber, payment_processor: Subscriber.payment_processors[:paypal]) }
  let(:subscriber_id) { subscriber.id }

  # Test suite for POST /paypal
  describe 'POST /paypal' do
    let(:valid_attributes) { { subscriber_id: subscriber_id, amount: '10.00', transaction_type: 'renewal' } }

    before { post "/paypal", params: valid_attributes }

    context 'when request is valid' do
      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end
end
