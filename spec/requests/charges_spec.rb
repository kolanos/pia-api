require 'rails_helper'

RSpec.describe 'Charges API' do
  # Initialize the test data
  let!(:subscriber) { create(:subscriber) }
  let!(:charges) { create_list(:charge, 20, subscriber_id: subscriber.id) }
  let(:subscriber_id) { subscriber.id }
  let(:id) { charges.first.id }

  # Test suite for GET /subscribers/:subscriber_id/charges
  describe 'GET /subscribers/:subscriber_id/charges' do
    before { get "/subscribers/#{subscriber_id}/charges" }

    context 'when subscriber exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns all subscriber charges' do
        expect(json.size).to eq(20)
      end
    end

    context 'when subscriber does not exist' do
      let(:subscriber_id) { 0 }

      it 'returns status code 403' do
        expect(response).to have_http_status(403)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Subscriber/)
      end
    end
  end

  # Test suite for GET /subscribers/:subscriber_id/charges/:id
  describe 'GET /subscribers/:subscriber_id/charges/:id' do
    before { get "/subscribers/#{subscriber_id}/charges/#{id}" }

    context 'when subscriber charge exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns the charge' do
        expect(json['id']).to eq(id)
      end
    end

    context 'when subscriber charge does not exist' do
      let(:id) { 0 }

      it 'returns status code 403' do
        expect(response).to have_http_status(403)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Charge/)
      end
    end
  end

  # Test suite for POST /subscribers/:subscriber_id/charges
  describe 'POST /subscribers/:subscriber_id/charges' do
    let(:valid_attributes) { { amount: '100.00' } }

    context 'when request attributes are valid' do
      before { post "/subscribers/#{subscriber_id}/charges", params: valid_attributes }

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when an invalid request' do
      before { post "/subscribers/#{subscriber_id}/charges", params: {} }

      it 'returns status code 403' do
        expect(response).to have_http_status(403)
      end

      it 'returns a failure message' do
        expect(response.body).to match(/Validation failed: Amount can't be blank/)
      end
    end
  end
end
