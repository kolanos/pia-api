
FactoryGirl.define do
  factory :charge do
    subscriber_id nil
    amount { Faker::Number.decimal(10, 2) }
    transaction_type 0
    processed false
  end
end
