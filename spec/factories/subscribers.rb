FactoryGirl.define do
  factory :subscriber do
    email { Faker::Internet.email }
    payment_processor 0
    payment_plan 0
  end
end
