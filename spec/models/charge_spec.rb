require 'rails_helper'

RSpec.describe Charge, type: :model do
  it { should belong_to(:subscriber) }
  it { should validate_presence_of(:amount) }
end
