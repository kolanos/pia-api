require 'rails_helper'

RSpec.describe Subscriber, type: :model do
  it { should have_many(:charges).dependent(:destroy) }
  it { should validate_presence_of(:email) }
end
