require 'time'

desc 'Bill Subscribers'
task :bill_subscribers => :environment do
  puts "Billing subscribers..."

  Subscriber.where('(next_bill IS NULL OR next_bill <= ?) AND payment_processor = ?', Date.today, Subscriber.payment_processors[:creditcard]).find_each do |subscriber|
    subscriber.bill
  end
end 
