desc 'Process Charges'
task :process_charges => :environment do
  puts "Processing charges..."
  Charge.where(processed: false).find_each do |charge|
    charge.process
  end
end 
