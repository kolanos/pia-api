require 'net/http'
require 'uri'

class CreditCardProcessor
  def initialize
    @base_url = 'http://localhost:3000'
  end

  def signup(email, amount)
    params = {'email' => email, 'amount' => amount}
    Net::HTTP.post_form(URI.parse(@base_url + '/signup'), params).code != "403"
  end

  def charge(subscriber, amount)
    params = {'amount' => amount}
    Net::HTTP.post_form(URI.parse(@base_url + '/charge/' + subscriber[:id].to_s), params).code != "403"
  end
end
